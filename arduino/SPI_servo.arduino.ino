#include <Servo.h>
#include <Wire.h>

Servo myservo;

#define SLAVE_ADD 0x08
#define SERVO   9

volatile int mode = 0;
int prevMode = 0;
int servoPos = 0;
int targetServoPos = 0;
long currentMillis = 0;
long previousMillis = 0;
int interval = 50;

void setup()
{

  myservo.attach(SERVO);

  Wire.begin(SLAVE_ADD);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
  Serial.begin(9600);
  previousMillis = millis();
}

void loop()
{
  currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;

    if (servoPos < targetServoPos) {
      servoPos += 2;
    }
    else if (servoPos > targetServoPos) {
      servoPos -= 2;
    }
    myservo.write(servoPos);
  }
}

// function that e192.168.254.125xecutes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany)
{
  int inData = Wire.read(); // receive byte as an integer
  Serial.println(inData); // print the integer
  targetServoPos = inData;
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent()
{
  
}
